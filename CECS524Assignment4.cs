﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace CECS524Assignment4
{
    class XMLFromFiles
    {
        // public static variables that will be used in the entirety of the program

        // 3 lists that will keep track of the file info, file types, and the file details
        // that will be in the XML output
        public static List<FileInfo> fileInfos = new List<FileInfo>();
        public static List<string> fileTypes = new List<string>();
        public static List<XMLOutputDetails> fileDetails = new List<XMLOutputDetails>();

        // 2 command line arguments: the path and the output file name
        public static string localPath;
        public static string outputFile;

        public static void Main(string[] args)
        {
            // Assign the local path and the output file to the command line arguments
            localPath = args[0];
            outputFile = args[1];

            // Call the implemented methods to create XML from files.
            CreateReport(EnumerateFilesRecursively(localPath));

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }


        /*
         * Enumerate all files in a given folder recursively including the entire sub-folder hierarchy.
           Uses the generator pattern (yield keyword) to implement the iterator. After generating, uses
           a helper function "ReturnInfoFromFiles" to populate the fileTypes list with the file
           information from the current file.
         */
        static IEnumerable<string> EnumerateFilesRecursively(string path)
        {     
            foreach (string fileInDirectory in Directory.EnumerateFiles(@path, "*", SearchOption.AllDirectories))
            {
                foreach (string filename in EnumerateFilesRecursively(fileInDirectory))
                {                   
                    yield return filename;

                    foreach (FileInfo info in ReturnInfoFromFiles(filename))
                    {
                        fileInfos.Add(info);
                        if (!fileTypes.Contains(info.Extension.ToLower()))
                        {
                            fileTypes.Add(info.Extension.ToLower());
                        }
                    }
                }
            }
        }

        // Helper function to return FileInfo from the path
        static IEnumerable<FileInfo> ReturnInfoFromFiles(string path)
        {
            FileInfo fileInfo = new FileInfo(@path);
            yield return fileInfo;
        }

        /*
           Format a byte size in human readable form. Use the following units: B, kB, MB, GB, TB, PB,
           EB, and ZB where 1kB = 1000B. The numerical value should be greater or equal to 1, less
           than 1000 and formatted with 2 fixed digits after the decimal point, e.g. 1.30kB. 
         */
        static string FormatByteSize(long byteSize)
        {
            string[] byteType = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };

            if (byteSize == 0)
                return "0" + byteType[0];

            long bytes = Math.Abs(byteSize);

            int byteTypeIndex = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));

            double num = Math.Round(bytes / Math.Pow(1024, byteTypeIndex), 1);

            return (Math.Sign(byteSize) * num).ToString() + byteType[byteTypeIndex];
        }

        /*
            Create a HTML document containing a table with three columns: “Type”, “Count”, and “Size”
            for the file name extension (converted to lower case), the number of files with this type, and
            the total size of all files with this type, respectively.
            Uses System.IO.FileInfo to get the size of a file with a given path.
            Sort the table by the byte size value of the “Size” column in descending order.
            Use your FormatByteSize function to format the value printed in the “Size” column.
            Implements this function using LINQ queries making use of group by and orderby.
            Uses the System.Xml.Linq.XElement constructor to functionally construct the XML
            document.
         */
        static XDocument CreateReport(IEnumerable<string> files)
        {
            // Nested foreach loop to count the number of files of each type.
            // Update the total size after if the types match
            foreach (var type in fileTypes)
            {
                var typeCount = 0;
                long totalSize = 0;

                foreach (FileInfo info in fileInfos)
                {
                    if (type.Equals(info.Extension.ToLower()))
                    {
                        typeCount++;
                        totalSize += info.Length;
                    }
                }

                // Details of the XML output. Create an object and instantiate its details
                // Add it to the fileDetails list to be iterated on later.
                XMLOutputDetails newOutput = new XMLOutputDetails();
                newOutput.FileType = type;
                newOutput.FileTypeCount = typeCount;
                newOutput.FileTypeSize = FormatByteSize(totalSize);
                fileDetails.Add(newOutput);
            }

            // Writes to the output file
            TextWriter tw = new StreamWriter(@localPath + outputFile);

            // XDocument that will contain the return value 
            XDocument output = null;

            // Create an XML tree for each file detail in the fileDetail list.
            // The attributes contain the file type, file type count, adn the total size.
            // Pass the XML tree into the XDocument to return
            foreach (var detail in fileDetails)
            {
                XElement xmlTree = new XElement("Root",
                                 new XElement("FileType", detail.FileType),
                                 new XElement("FileCount", detail.FileTypeCount),
                                 new XElement("TotalSize", detail.FileTypeSize)
                                );
                output = new XDocument(xmlTree);
            }
            tw.Close();
            
            return output;
        }

        // Class to represent the details of the output that will be present in the XML tree.
        public class XMLOutputDetails
        {
            public string FileType { get; set; }
            public int FileTypeCount { get; set; }
            public string FileTypeSize { get; set; }
        }
    }
}
