# README #

### What is this repository for? ###

This application has two command line arguments: A path to a folder and a name for a
HTML report output file. The application collects all files with the same extension (converted
to lower case) and determines for each extension, i.e. file type, the number of files and the
total size of all files of this type.

This application is written in C#.


### Who do I talk to? ###

* Repo owner: Robien Marquez